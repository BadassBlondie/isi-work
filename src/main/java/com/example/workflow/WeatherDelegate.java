package com.example.workflow;

import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.*;
import java.util.stream.Collectors;

import static org.apache.logging.log4j.ThreadContext.removeAll;

public class WeatherDelegate implements JavaDelegate {


    @Override
    public void execute(DelegateExecution delegateExecution) throws Exception {
        List<String> cities = new ArrayList<>();
        if(delegateExecution.getVariable("cities")==null){
             new ArrayList<>();
        }else{
            String citiesS = (String) delegateExecution.getVariable("cities");
            String [] citiesArray = citiesS.split(",");
            cities= Arrays.asList(citiesArray);
        }


        Map<String, String> hashMap = new HashMap<>();

        hashMap.put("London",weatherDestinations("London,Uk"));
        hashMap.put("Barcelona",weatherDestinations("Barcelona,Spain"));
        hashMap.put("Sydney",weatherDestinations("Sydney,Australia"));
        hashMap.put("Valletta",weatherDestinations("Valletta,Malta"));

        while (hashMap.values().remove(null));
        System.out.println(hashMap.toString());

        if(!hashMap.isEmpty()) {
            delegateExecution.setVariable("accepted", false);
            delegateExecution.setVariable("destination", returnBestDestination(hashMap, delegateExecution,cities));

        }else
        {
            delegateExecution.setVariable("wait", "Wyszukaj połączenia jutro");
            delegateExecution.setVariable("accepted", true);

        }
    }

    private String weatherDestinations(String destination){
        try {
            URL url = new URL("https://community-open-weather-map.p.rapidapi.com/weather?q="+destination);
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod("GET");
            con.setRequestProperty("x-rapidapi-key","69b95a5722msh5451a139099eb04p15ef06jsne59d995e98d3");
            con.setRequestProperty("x-rapidapi-host","community-open-weather-map.p.rapidapi.com");
            con.setRequestProperty("useQueryString","true");

            int responseCode = con.getResponseCode();

            if (responseCode == HttpURLConnection.HTTP_OK) { // success
                BufferedReader in = new BufferedReader(new InputStreamReader(
                        con.getInputStream()));
                String inputLine;
                StringBuffer response = new StringBuffer();

                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }
                in.close();

                String responseBody = response.toString();
                JSONObject jsonObject = new JSONObject(responseBody);
                String details = jsonObject.get("main").toString();
                JSONObject jsonObjectTemp = new JSONObject(details);
                String temp = jsonObjectTemp.get("temp").toString();
                return temp;
            }else{
                System.out.println("Request failed "+getClass().getName());
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    private String returnBestDestination(Map hashmap,DelegateExecution delegateExecution,List<String> cities) {
        Map<String, String> map = hashmap;
        ArrayList<String> toRemove = new ArrayList();

        String city;
        if(cities.isEmpty()) {
            city = Collections.min(map.entrySet(), Map.Entry.comparingByValue()).getKey();
            delegateExecution.setVariable("firstCity", city);
            delegateExecution.setVariable("cities", city);
        }
        else{
            for (String entry : map.keySet()) {
                for(int j=0;j<cities.size();j++){
                    if (entry.equals(cities.get(j))) {
                        System.out.println("City do usuniecia - "+entry);
                        toRemove.add(entry);
                    }
                }
            }

            for (String key:toRemove
                 ) {
                map.remove(key);
            }

            System.out.println(map.toString());

            if(map.isEmpty()){
                delegateExecution.setVariable("wait", "Wyszukaj połączenia jutro");
                delegateExecution.setVariable("accepted", true);
                return  (String) delegateExecution.getVariable("firstCity");
            }
            city = Collections.min(map.entrySet(), Map.Entry.comparingByValue()).getKey();
            delegateExecution.setVariable("cities", delegateExecution.getVariable("cities")+","+city);

        }
        System.out.println("Lokalizacja city = "+city);
        return city;
    }
}
