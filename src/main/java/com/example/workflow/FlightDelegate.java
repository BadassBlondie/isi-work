package com.example.workflow;

import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.json.JSONArray;
import org.json.JSONObject;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class FlightDelegate implements JavaDelegate {

    @Override
    public void execute(DelegateExecution delegateExecution) throws Exception {


        //odebranie lokalizacji
        String city = (String) delegateExecution.getVariable("destination");

        //stale
        final String origin = "PL-sky";
        final String currency = "PLN";
        final String country = "PL";
        final String locale = "en-US";
        String IATAcode = findIataByCity(city);
        LocalDate date = LocalDate.now().plusDays(1);
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        String flightDate = date.format(formatter);
        //zmiana nazwy lokalizacji na kod
        String url = "https://skyscanner-skyscanner-flight-search-v1.p.rapidapi.com/apiservices/browsequotes/v1.0/" +
                country + "/" +
                currency + "/" +
                locale + "/" +
                origin + "/" +
                IATAcode + "/" +
                flightDate;

        Float cost = getFlightPrice(url);
        delegateExecution.setVariable("cost", cost.toString());

    }

    private float getFlightPrice(String link){
        float price=0;
        try {
            URL url = new URL(link);
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod("GET");
            con.setRequestProperty("x-rapidapi-key","3d3b824261msh69421dfd77d541ep127b04jsna2e8ea1d492b");
            con.setRequestProperty("x-rapidapi-host","skyscanner-skyscanner-flight-search-v1.p.rapidapi.com");


            int responseCode = con.getResponseCode();
            System.out.println("GET Response Code :: " + responseCode);
            if (responseCode == HttpURLConnection.HTTP_OK) { // success
                BufferedReader in = new BufferedReader(new InputStreamReader(
                        con.getInputStream()));
                String inputLine;
                StringBuffer response = new StringBuffer();
                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }
                in.close();


                String responseBody = response.toString();
                JSONObject jsonObject = new JSONObject(responseBody);
                JSONArray array = jsonObject.getJSONArray("Quotes");
                System.out.println("dl - "+array.length());

                if(!array.isEmpty()){
                    price = array.getJSONObject(0).getFloat("MinPrice");
                }

            }else
            {
                System.out.println("Request failed "+getClass().getName());
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return price;
    }


    String findIataByCity(String city){
        String iata="";
        switch(city){
            case "London":
                iata= "LOND-sky";
                break;
                case "Barcelona":
                    iata= "BCN-sky";
                break;
                case "Sydney":
                    iata= "SYD-sky";
                break;
                case "Valletta":
                    iata= "MLA-sky";
                break;
                case "Kuta":
                    iata= "KUT-sky";
                break;
                case "Kair":
                    iata= "CAI-sky";
                break;
        }
            return iata;
    }

}
